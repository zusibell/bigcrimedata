#Installation & Setup

Download and install Kafka from following website

https://kafka.apache.org/quickstart

#Run Kakfa
Start the zookeeper server first from the kafka folder
```
bin/zookeeper-server-start.sh config/zookeeper.properties
```
From another shell start the kafka server
```
bin/kafka-server-start.sh config/server.properties
```
From another shell, check that all following topics are stored
```
bin/kafka-topics.sh --describe --bootstrap-server localhost:9092
```
``` 
hate-crime-bias-anti-male-offense
hate-crime-bias-anti-female-offense
hate-crime-bias-anti-heterosexual-offense
hate-crime-bias-anti-transgender-offense
hate-crime-bias-anti-bisexual-offense
hate-crime-bias-anti-gay-mixed-offense
hate-crime-bias-anti-lesbian-offense
hate-crime-bias-anti-gay-male-offense
hate-crime-bias-anti-gender-non-conforming
 ```
Look at what's stored in a topics
```
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --bias {bias} --from-beginning
```

On Windows it might work from the Git Bash but I installed [WSL](https://docs.microsoft.com/en-us/windows/wsl/install).
