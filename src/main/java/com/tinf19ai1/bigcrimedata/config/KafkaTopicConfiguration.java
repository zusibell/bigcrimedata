package com.tinf19ai1.bigcrimedata.config;

import com.tinf19ai1.bigcrimedata.models.Bias;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfiguration {
    @Bean
    public NewTopic hateCrimeBiasAntiMaleOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_MALE_OFFENSE.getTopicName(),1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiFemaleOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_FEMALE_OFFENSE.getTopicName(),1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiHeterosexualOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_HETEROSEXUAL_OFFENSE.getTopicName(), 1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiTransgenderOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_TRANSGENDER_OFFENSE.getTopicName(), 1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiBisexualOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_BISEXUAL_OFFENSE.getTopicName(), 1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiGayMixedOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_GAY_MIXED_OFFENSE.getTopicName(), 1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiLesbianOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_LESBIAN_OFFENSE.getTopicName(), 1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiGayMaleOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_GAY_MALE_OFFENSE.getTopicName(), 1, (short) 1);
    }
    @Bean
    public NewTopic hateCrimeBiasAntiGenderNonConformingOffense() {
        return new NewTopic(Bias.HATE_CRIME_BIAS_ANTI_GENDER_NON_CONFORMING_OFFENSE.getTopicName(), 1, (short) 1);
    }
}
