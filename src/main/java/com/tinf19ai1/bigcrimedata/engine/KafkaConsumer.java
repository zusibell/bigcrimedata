package com.tinf19ai1.bigcrimedata.engine;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.tinf19ai1.bigcrimedata.models.Bias;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class KafkaConsumer {
    @Value(value = "${spring.data.mongodb.uri}")
    private String mongodbUri;

    private MongoDatabase getDatabase() {
        MongoClient mongoClient = MongoClients.create(mongodbUri);
        return mongoClient.getDatabase("fbi-crime-data");
    }

    @KafkaListener(topics = "hate-crime-bias-anti-male-offense")
    public void consumeHateCrimeBiasAntiMaleOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_MALE_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }

    @KafkaListener(topics = "hate-crime-bias-anti-female-offense")
    public void consumeHateCrimeBiasAntiFemaleOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_FEMALE_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }

    @KafkaListener(topics = "hate-crime-bias-anti-heterosexual-offense")
    public void consumeHateCrimeBiasAntiHeterosexualOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_HETEROSEXUAL_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }

    @KafkaListener(topics = "hate-crime-bias-anti-transgender-offense")
    public void consumeHateCrimeBiasAntiTransgenderOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_TRANSGENDER_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }

    @KafkaListener(topics = "hate-crime-bias-anti-bisexual-offense")
    public void consumeHateCrimeBiasAntiBisexualOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_BISEXUAL_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }
    @KafkaListener(topics = "hate-crime-bias-anti-gay-mixed-offense")
    public void consumeHateCrimeBiasAntiGayMixedOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_GAY_MIXED_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }
    @KafkaListener(topics = "hate-crime-bias-anti-lesbian-offense")
    public void consumeHateCrimeBiasAntiLesbianOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_LESBIAN_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }
    @KafkaListener(topics = "hate-crime-bias-anti-gay-male-offense")
    public void consumeHateCrimeBiasAntiGayMaleOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_GAY_MALE_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }
    @KafkaListener(topics = "hate-crime-bias-anti-gender-non-conforming-offense")
    public void consumeHateCrimeBiasAntiGenderNonConformingOffense(String message) {
        getDatabase().getCollection(Bias.HATE_CRIME_BIAS_ANTI_GENDER_NON_CONFORMING_OFFENSE.getTopicName()).insertOne(Document.parse(message).append("added_date", LocalDateTime.now()));
    }
}
