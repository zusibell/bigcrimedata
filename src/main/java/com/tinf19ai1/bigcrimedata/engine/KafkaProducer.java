package com.tinf19ai1.bigcrimedata.engine;

import com.tinf19ai1.bigcrimedata.models.Bias;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class KafkaProducer {
    private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    @Value(value="${fbi.crime.data.api.key}")
    private String apiKey;
    @Value(value="${hate.crime.base.url}")
    private String baseUrl;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendRequest(Bias bias) {
        logger.info(String.format("#### -> Producing message with topic -> %s", bias));
        String data = getData(bias.getBiasApiPath());
        this.kafkaTemplate.send(bias.getTopicName(), data);
    }

    public String getData(String path) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(String.format("%s%s?api_key=%s",baseUrl, path, apiKey), String.class);
    }

}
