package com.tinf19ai1.bigcrimedata.models;

public enum Bias {
    HATE_CRIME_BIAS_ANTI_MALE_OFFENSE("hate-crime-bias-anti-male-offense", "/api/hatecrimebybias/national/anti-male/offense"),
    HATE_CRIME_BIAS_ANTI_FEMALE_OFFENSE("hate-crime-bias-anti-female-offense", "/api/hatecrimebybias/national/anti-female/offense"),
    HATE_CRIME_BIAS_ANTI_HETEROSEXUAL_OFFENSE("hate-crime-bias-anti-heterosexual-offense", "/api/hatecrimebybias/national/anti-heterosexual/offense"),
    HATE_CRIME_BIAS_ANTI_TRANSGENDER_OFFENSE("hate-crime-bias-anti-transgender-offense", "/api/hatecrimebybias/national/anti-transgender/offense"),
    HATE_CRIME_BIAS_ANTI_BISEXUAL_OFFENSE("hate-crime-bias-anti-bisexual-offense", "/api/hatecrimebybias/national/anti-bisexual/offense"),
    HATE_CRIME_BIAS_ANTI_GAY_MIXED_OFFENSE("hate-crime-bias-anti-gay-mixed-offense", "/api/hatecrimebybias/national/anti-gay-mixed/offense"),
    HATE_CRIME_BIAS_ANTI_LESBIAN_OFFENSE("hate-crime-bias-anti-lesbian-offense", "/api/hatecrimebybias/national/anti-lesbian/offense"),
    HATE_CRIME_BIAS_ANTI_GAY_MALE_OFFENSE("hate-crime-bias-anti-gay-male-offense", "/api/hatecrimebybias/national/anti-gay-male/offense"),
    HATE_CRIME_BIAS_ANTI_GENDER_NON_CONFORMING_OFFENSE("hate-crime-bias-anti-gender-non-conforming-offense", "/api/hatecrimebybias/national/anti-gender-non-conforming/offense");

    private final String topicName;
    private final String biasApiPath;

    Bias(String topic, String urlPath) {
        this.topicName = topic;
        this.biasApiPath = urlPath;
    }

    public String getTopicName() {
        return this.topicName;
    }

    public String getBiasApiPath() {return this.biasApiPath;}
}
