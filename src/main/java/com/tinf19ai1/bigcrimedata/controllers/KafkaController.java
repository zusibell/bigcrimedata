package com.tinf19ai1.bigcrimedata.controllers;

import com.tinf19ai1.bigcrimedata.engine.KafkaProducer;
import com.tinf19ai1.bigcrimedata.models.Bias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class KafkaController {

    private final KafkaProducer producer;

    @Autowired
    KafkaController(KafkaProducer producer) {
        this.producer = producer;
    }

    @PostMapping(value = "/refresh")
    public String sendMessageToHateCrimeBiasLocation(@RequestParam("bias") Bias bias) {
        this.producer.sendRequest(bias);
        return "redirect:/";
    }

    @RequestMapping(value="/refresh/all")
    public String refreshAllData(){
        for (Bias bias : Bias.values()) {
            this.producer.sendRequest(bias);
        }
        return "redirect:/";
    }

}
