package com.tinf19ai1.bigcrimedata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigCrimeDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(BigCrimeDataApplication.class, args);
	}

}
