FROM maven:3.8-jdk-11 AS MAVEN_BUILD
COPY ./ ./
RUN mvn clean package -DskipTests

FROM openjdk:11
COPY --from=MAVEN_BUILD target/*.jar bigcrimedata.jar
CMD ["java","-jar","bigcrimedata.jar"]