# BigCrimeData

FBI Crime Data pipeline and visualization project.

This project can be started with docker by running the following command inside the project.
```
docker-compose up
```

It can also be completely locally started, by starting Kafka first (see [Kafka Quickstart](#kafka-quickstart)) and then starting Spring Boot.

# Kafka Quickstart
https://kafka.apache.org/quickstart

See [Kafka Intro](KAFKA.md)

# Kafka Topics & URLs
```
 hate-crime-bias-anti-male-offense              /api/hatecrimebybias/national/anti-male/offense
 hate-crime-bias-anti-female-offense            /api/hatecrimebybias/national/anti-female/offense
 hate-crime-bias-anti-heterosexual-offense      /api/hatecrimebybias/national/anti-heterosexual/offense
 hate-crime-bias-anti-transgender-offense       /api/hatecrimebybias/national/anti-transgender/offense
 hate-crime-bias-anti-bisexual-offense          /api/hatecrimebybias/national/anti-bisexual/offense
 hate-crime-bias-anti-gay-mixed-offense         /api/hatecrimebybias/national/anti-gay-mixed/offense
 hate-crime-bias-anti-lesbian-offense           /api/hatecrimebybias/national/anti-lesbian/offense
 hate-crime-bias-anti-gay-male-offense          /api/hatecrimebybias/national/anti-gay-male/offense
 hate-crime-bias-anti-gender-non-conforming     /api/hatecrimebybias/national/anti-gender-non-conforming/offense
```

